# git-test

Interactive git exercises

## Exercise 1

1. Create new branch following convention 
```
exercise_1/<surname>.<name>
```
2. Change the last commit on the branch correcting both code mistake and commit message
3. Push changes to remote

## Exercise 2

1. Undo the last commit preserving both the commit and the undo in history
2. Push changes to remote

## Exercise 3

1. Undo the previously commited revert, removing it from history, like, forever
2. Also, change the commit message of previous commit from:
```
Added instructions in readme
```
to 
```
Added instructions in .README
```

## Exercise 4

1. Merge branch exercise_4 to your branch and resolve conflicts
2. Then, undo the merge, removing the merge commit and restoring the repo to state from before the merge
3. Rebase to exercise_4 and resolve conflicts
